import XCTest
@testable import RevolutDemo

class ExchangeRatesBundleTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testSettingRate() {
        var ratesBox = ExchangeRatesBundle(base: "EUR", actualDate: Date())
        let currencyCode = "USD"
        let exchangeRate = 1.1675
        
        let rateAccepted = ratesBox.setRate(exchangeRate, forCurrency: currencyCode)
        XCTAssertTrue(rateAccepted)
        
        let storedRate = ratesBox.getRate(forCurrency: currencyCode)
        XCTAssertNotNil(storedRate, "Exchange rate not found!")
        XCTAssertEqual(storedRate!, exchangeRate, "Value did not match the expected exchange rate!")
    }
    
    func testSettingSelfRateIsNotAccepted() {
        let baseCurrency = "EUR"
        var ratesBox = ExchangeRatesBundle(base: baseCurrency, actualDate: Date())
        
        let rateAccepted = ratesBox.setRate(1.01, forCurrency: baseCurrency)
        XCTAssertFalse(rateAccepted, "Cheating with base currency rate is not acceptable!")
        
        let eureur = ratesBox.getRate(forCurrency: baseCurrency)
        XCTAssertNil(eureur, "Cheating with base currency rate is not acceptable!")
    }
    
    func testSettingNegativeOrZeroRateIsNotAccepted() {
        let exchangeCurrency = "CNY"
        var ratesBox = ExchangeRatesBundle(base: "EUR", actualDate: Date())
        
        let negRateAccepted = ratesBox.setRate(-7.93, forCurrency: exchangeCurrency)
        XCTAssertFalse(negRateAccepted, "Negative rates are not acceptable!")
        
        let zeroRateAccepted = ratesBox.setRate(0.0, forCurrency: exchangeCurrency)
        XCTAssertFalse(zeroRateAccepted, "Zero rates are not acceptable!")
        
        let storedRate = ratesBox.getRate(forCurrency: exchangeCurrency)
        XCTAssertNil(storedRate)
    }
    
    func testUpdatingRate() {
        var ratesBox = ExchangeRatesBundle(base: "USD", actualDate: Date())
        let currencyCode = "RUB"
        let exchangeRateOld = 65.7200
        let exchangeRateNew = 65.7500
        
        ratesBox.setRate(exchangeRateOld, forCurrency: currencyCode)
        ratesBox.setRate(exchangeRateNew, forCurrency: currencyCode)
        
        let storedRate = ratesBox.getRate(forCurrency: currencyCode)
        XCTAssertEqual(storedRate!, exchangeRateNew, "Value did not match the latest exchange rate!")
    }
    
    func testRebaseFailsForNonListedCurrency() {
        var eurRatesBox = ExchangeRatesBundle(base: "EUR", actualDate: Date())
        eurRatesBox.setRate(1.25, forCurrency: "USD")
        
        let sekRatesBox = eurRatesBox.rebased(to: "SEK")
        XCTAssertNil(sekRatesBox, "Cannot re-base since no rate for 'SEK' has been listed")
    }
    
    func testFakeRebaseMakesCopy() {
        let constBase = "EUR"
        var ratesBox = ExchangeRatesBundle(base: constBase, actualDate: Date())
        ratesBox.setRate(1.25, forCurrency: "USD")
        ratesBox.setRate(9.95, forCurrency: "SEK")
        
        let carelesslyRebased = ratesBox.rebased(to: constBase)
        XCTAssertNotNil(carelesslyRebased)
        
        let areEqual = !ExchangeRatesBundle.diff(lhs: ratesBox, rhs: carelesslyRebased!)
        XCTAssertTrue(areEqual, "Must not be modified upon fake rebase!")
    }
    
    func testRebasingOutline() {
        var eurRatesBox = ExchangeRatesBundle(base: "EUR", actualDate: Date())
        eurRatesBox.setRate(1.25, forCurrency: "USD")
        eurRatesBox.setRate(75, forCurrency: "RUB")
        
        let usdRatesBox = eurRatesBox.rebased(to: "USD")
        XCTAssertNotNil(usdRatesBox, "Re-basing must succeed for a listed currency!")
        XCTAssertEqual(usdRatesBox!.baseCurrency, "USD", "Must change base currency upon rebase!")
        XCTAssertEqual(usdRatesBox!.date, eurRatesBox.date, "Must keep date upon rebase!")
        XCTAssertEqual(usdRatesBox!.count, eurRatesBox.count, "Number of listed currencies must not change upon rebase!")
    }
    
    func testRatesAreRecalculatedUponRebase() {
        let eurusd: Double = 2.0
        let eurrub: Double = 50.0
        
        let exp_rubeur = 1 / eurrub
        let exp_rubusd = eurusd / eurrub

        var eurRatesBox = ExchangeRatesBundle(base: "EUR", actualDate: Date())
        eurRatesBox.setRate(eurusd, forCurrency: "USD")
        eurRatesBox.setRate(eurrub, forCurrency: "RUB")

        let rubRatesBox = eurRatesBox.rebased(to: "RUB")

        let rubeur = rubRatesBox?.getRate(forCurrency: "EUR")
        let rubusd = rubRatesBox?.getRate(forCurrency: "USD")

        XCTAssertEqual(rubeur, exp_rubeur, "Incorrect converted rate")
        XCTAssertEqual(rubusd, exp_rubusd, "Incorrect converted rate")
    }
    
    func testBundlesAreEqualHoldingSameCurrencies() {
        let eurusd1: Double = 2.0, eurusd2 = 2.25
        let eurrub1: Double = 50.0, eurrub2 = 75.0
        let eursek1: Double = 9.75, eursek2 = 10.0
        
        var eurRatesBox = ExchangeRatesBundle(base: "EUR", actualDate: Date())
        eurRatesBox.setRate(eurusd1, forCurrency: "USD")
        eurRatesBox.setRate(eurrub1, forCurrency: "RUB")
        eurRatesBox.setRate(eursek1, forCurrency: "SEK")
        
        var newRatesBox = ExchangeRatesBundle(base: "EUR", actualDate: Date())
        newRatesBox.setRate(eurusd2, forCurrency: "USD")
        newRatesBox.setRate(eurrub2, forCurrency: "RUB")
        newRatesBox.setRate(eursek2, forCurrency: "SEK")
        
        let hasChanged = ExchangeRatesBundle.diff(lhs: eurRatesBox, rhs: newRatesBox)
        
        XCTAssertFalse(hasChanged, "Must remain equal if not changing the involved currencies!")
    }
    
    func testBundlesAreNonEqualHavingDifferentBase() {
        let t = Date()
        let eurRatesBox = ExchangeRatesBundle(base: "EUR", actualDate: t)
        let usdRatesBox = ExchangeRatesBundle(base: "USD", actualDate: t)
        
        let diff = ExchangeRatesBundle.diff(lhs: eurRatesBox, rhs: usdRatesBox)
        
        XCTAssertTrue(diff, "Must be non-equal having different base currencies!")
    }
    
    func testBundlesAreNonEqualWhenComposedOfDifferentCurrencies() {
        let eurusd: Double = 2.0
        let eurrub: Double = 50.0
        let eursek: Double = 9.75
        
        var eurRatesBox = ExchangeRatesBundle(base: "EUR", actualDate: Date())
        eurRatesBox.setRate(eurusd, forCurrency: "USD")
        eurRatesBox.setRate(eurrub, forCurrency: "RUB")
        
        var extendedRatesBox = eurRatesBox
        extendedRatesBox.setRate(eursek, forCurrency: "SEK")
        let diffEx = ExchangeRatesBundle.diff(lhs: eurRatesBox, rhs: extendedRatesBox)
        XCTAssertTrue(diffEx, "Must have changed!")
        
        var shrinkedRatesBox = ExchangeRatesBundle(base: "EUR", actualDate: Date())
        shrinkedRatesBox.setRate(eurusd, forCurrency: "USD")
        let diffShrinked = ExchangeRatesBundle.diff(lhs: eurRatesBox, rhs: shrinkedRatesBox)
        XCTAssertTrue(diffShrinked, "Must have changed!")
        
        shrinkedRatesBox.setRate(eursek, forCurrency: "SEK")
        let diffChanged = ExchangeRatesBundle.diff(lhs: eurRatesBox, rhs: shrinkedRatesBox)
        XCTAssertTrue(diffChanged, "Must have changed!")
    }


}
