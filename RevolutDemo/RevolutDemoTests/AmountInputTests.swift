import XCTest
@testable import RevolutDemo

class AmountInputTests: XCTestCase {

    var amountValidator: InputValidator!
    
    override func setUp() {
        amountValidator = AmountInputValidator()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testInputNotValidHavingNonDigitSymbols() {
        let unwantedInputs = [ "€87.50", "3.25 €", "-200.00", "1 490.00", "54.-" ]
        
        for inputString in unwantedInputs {
            let inputIsValid = amountValidator.isValidInput(inputString)
            XCTAssertFalse(inputIsValid, "Must not be valid when having unwanted symbols!")
        }
    }
    
    func testInputNotValidIfStartedWithDelimeter() {
        let unwantedInputs = [ ".625" ]
        
        for inputString in unwantedInputs {
            let inputIsValid = amountValidator.isValidInput(inputString)
            XCTAssertFalse(inputIsValid, "Amount must not start with a delimeter!")
        }
    }
    
    func testInputNotValidHavingMultipleDelimeters() {
        let unwantedInputs = [ "250..00", "74.90." ]
        
        for inputString in unwantedInputs {
            let inputIsValid = amountValidator.isValidInput(inputString)
            XCTAssertFalse(inputIsValid, "Must not be valid when having multiple delimeters!")
        }
    }
    
    func testInputValidWhenRespectsFractionalLengthLimit() {
        let tunedAmountValidator = AmountInputValidator(with: nil, fracMaxLength: 4)!
        let maxFracLength = tunedAmountValidator.maxFractionalDigits
        var inputString = "25."
        
        for _ in 0...maxFracLength {
            let inputIsValid = tunedAmountValidator.isValidInput(inputString)
            XCTAssertTrue(inputIsValid, "Must be valid if fractional part does not exceed its length limit!")
            inputString.append("0")
        }
        
        let finalInputIsValid = tunedAmountValidator.isValidInput(inputString)
        XCTAssertFalse(finalInputIsValid, "Must not be valid if fractional part exceeds its length limit!")
    }
    
    func testInputValidWhenRespectsIntegerLengthLimit() {
        let tunedAmountValidator = AmountInputValidator(with: 99999)!
        guard let maxIntLength = tunedAmountValidator.maxIntegerDigits else {
            XCTFail("Integer part length limitation must be set along with the amount limit!")
            return
        }
        
        var inputString = "."
        for _ in 0..<maxIntLength {
            inputString.insert("1", at: inputString.startIndex)
            let inputIsValid = tunedAmountValidator.isValidInput(inputString)
            XCTAssertTrue(inputIsValid, "Must be valid if integer part does not exceed its length limit!")
        }
        
        inputString.insert("1", at: inputString.startIndex)
        let inputIsValid = tunedAmountValidator.isValidInput(inputString)
        XCTAssertFalse(inputIsValid, "Must not be valid if integer part exceeds its length limit!")
        
        inputString.removeFirst()
        while !inputString.isEmpty {
            inputString.removeLast()
            let inputIsValid = tunedAmountValidator.isValidInput(inputString)
            XCTAssertTrue(inputIsValid, "Must be valid if integer part does not exceed its length limit!")
        }
    }
    
    func testInputNotValidWithLeadingZeroNotFollowedByDelimeter() {
        let singleLeadingZeroInputs = [ "0", "0.", "0.47" ]
        for inputString in singleLeadingZeroInputs {
            let inputIsValid = amountValidator.isValidInput(inputString)
            XCTAssertTrue(inputIsValid, "Must be valid when starting with a single leading zero!")
        }
        
        let invalidLeadingZeroInputs = [ "00.47", "00", "09", "0075", "0001", "07.95", "030.25" ]
        for inputString in invalidLeadingZeroInputs {
            let inputIsValid = amountValidator.isValidInput(inputString)
            XCTAssertFalse(inputIsValid, "Must not be valid when starting with multiple leading zeros!")
        }
    }
    
    func testInputNotValidHavingValueAboveLimit() {
        let tunedAmountValidator = AmountInputValidator(with: 10000)!
        let okInputStrings = [ "9999", "9999.", "9999.0", "10000", "10000.00" ]
        for inputString in okInputStrings {
            let inputIsValid = tunedAmountValidator.isValidInput(inputString)
            XCTAssertTrue(inputIsValid, "Must be valid when having value below or equal to the limit!")
        }
        
        let nokInputStrings = [ "10000.01", "10001", "25000." ]
        for inputString in nokInputStrings {
            let inputIsValid = tunedAmountValidator.isValidInput(inputString)
            XCTAssertFalse(inputIsValid, "Must not be valid when having value above the limit!")
        }
    }
    
}
