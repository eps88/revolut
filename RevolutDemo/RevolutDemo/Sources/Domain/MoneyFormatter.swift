import Foundation

protocol AmountFormatter: class {
    func amountString(from amount: Double) -> String
}

protocol MoneyFormatter: AmountFormatter {
    func moneyString(from money: Money) -> String
}
