import Foundation

/**
 * User input validation contract.
 */
protocol InputValidator: class {
    
    /**
     * Check if the input string provided by a user is valid or not.
     */
    func isValidInput(_ inputString: String) -> Bool
}
