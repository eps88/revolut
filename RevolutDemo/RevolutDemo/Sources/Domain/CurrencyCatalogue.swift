import Foundation

/**
 * The static catalogue of most common world currencies.
 *
 * - Note: More complete info can be retrieved from a network resource
 *      however is considered to be out of the scope for now.
 */
final class CurrencyCatalogue {
    
    private static let roster: [String: CurrencyInfo] = {
        var list = [String: CurrencyInfo]()
        
        list["AMD"] = CurrencyInfo(code: "AMD", name: "Armenian Dram", symbol: "֏")
        list["ARS"] = CurrencyInfo(code: "ARS", name: "Argentine Peso", symbol: "$")
        list["AUD"] = CurrencyInfo(code: "AUD", name: "Australian Dollar", symbol: "$")
        list["BGN"] = CurrencyInfo(code: "BGN", name: "Bulgarian Lev", symbol: "лв")
        list["BRL"] = CurrencyInfo(code: "BRL", name: "Brazilian Real", symbol: "R$")
        list["CAD"] = CurrencyInfo(code: "CAD", name: "Canadian Dollar", symbol: "$")
        list["CHF"] = CurrencyInfo(code: "CHF", name: "Swiss Frank", symbol: "Fr")
        list["CNY"] = CurrencyInfo(code: "CNY", name: "Chinese Yuan", symbol: "¥")
        list["CZK"] = CurrencyInfo(code: "CZK", name: "Czech Koruna", symbol: "Kč")
        list["DKK"] = CurrencyInfo(code: "DKK", name: "Danish Krone", symbol: "kr")
        list["EUR"] = CurrencyInfo(code: "EUR", name: "Euro", symbol: "€")
        list["GBP"] = CurrencyInfo(code: "GBP", name: "British Pound", symbol: "£")
        list["GEL"] = CurrencyInfo(code: "GEL", name: "Georgian Lari", symbol: "₾")
        list["HKD"] = CurrencyInfo(code: "HKD", name: "Hong Kong Dollar", symbol: "$")
        list["HRK"] = CurrencyInfo(code: "HRK", name: "Croatian Kuna", symbol: "kn")
        list["HUF"] = CurrencyInfo(code: "HUF", name: "Hungarian Forint", symbol: "Ft")
        list["IDR"] = CurrencyInfo(code: "IDR", name: "Indonesian Rupiah", symbol: "Rp")
        list["ILS"] = CurrencyInfo(code: "ILS", name: "Israeli New Shekel", symbol: "₪")
        list["INR"] = CurrencyInfo(code: "INR", name: "Indian Rupee", symbol: "₹")
        list["ISK"] = CurrencyInfo(code: "ISK", name: "Icelandic Króna", symbol: "kr")
        list["JPY"] = CurrencyInfo(code: "JPY", name: "Japanese Yen", symbol: "¥")
        list["KZT"] = CurrencyInfo(code: "KZT", name: "Kazakhstani Tenge", symbol: "₸")
        list["KRW"] = CurrencyInfo(code: "KRW", name: "South Korean Won", symbol: "₩")
        list["MXN"] = CurrencyInfo(code: "MXN", name: "Mexican Peso", symbol: "$")
        list["MYR"] = CurrencyInfo(code: "MYR", name: "Malaysian Ringgit", symbol: "RM")
        list["NOK"] = CurrencyInfo(code: "NOK", name: "Norwegian Krone", symbol: "kr")
        list["NZD"] = CurrencyInfo(code: "NZD", name: "New Zealand Dollar", symbol: "$")
        list["PHP"] = CurrencyInfo(code: "PHP", name: "Philippine Piso", symbol: "₱")
        list["PLN"] = CurrencyInfo(code: "PLN", name: "Polish złoty", symbol: "zł")
        list["RON"] = CurrencyInfo(code: "RON", name: "Romanian Leu", symbol: "lei")
        list["RUB"] = CurrencyInfo(code: "RUB", name: "Russian Ruble", symbol: "₽")
        list["SGD"] = CurrencyInfo(code: "SGD", name: "Singapore Dollar", symbol: "$")
        list["SEK"] = CurrencyInfo(code: "SEK", name: "Swedish Krona", symbol: "kr")
        list["THB"] = CurrencyInfo(code: "THB", name: "Thai Baht", symbol: "฿")
        list["TRY"] = CurrencyInfo(code: "TRY", name: "Turkish Lira", symbol: "₺")
        list["USD"] = CurrencyInfo(code: "USD", name: "United States Dollar", symbol: "$")
        list["ZAR"] = CurrencyInfo(code: "ZAR", name: "South African Rand", symbol: "R")
        
        return list
    }()
    
    static func currencyByCode(_ code: String) -> CurrencyInfo? {
        return roster[code]
    }
    
}
