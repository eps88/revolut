import Foundation

/**
 * A list of currency exchange rates related to the specified base currency
 *  and valid as of the specified date.
 */
struct ExchangeRatesBundle {
    
    let baseCurrency: String
    let date: Date
    
    private(set) var rates: [String: Double] = [:]
    
    init(base: String, actualDate: Date) {
        self.baseCurrency = base
        self.date = actualDate
    }
    
    var count: Int {
        return rates.count
    }
    
    func getRate(forCurrency: String) -> Double? {
        return rates[forCurrency]
    }
    
    @discardableResult
    mutating func setRate(_ rate: Double, forCurrency: String) -> Bool {
        guard forCurrency != self.baseCurrency else {
            return false
        }
        
        guard rate > 0 else {
            return false
        }
        
        rates[forCurrency] = rate
        
        return true
    }

    /**
     * Make another exchange rates bundle having the specified base currency.
     *
     * - Parameter currency: The currency code for the base currency of the rebased bundle
     */
    func rebased(to currency: String) -> ExchangeRatesBundle? {
        guard currency != baseCurrency else {
            return self
        }
        
        guard let directRate = rates[currency] else {
            return nil
        }
        
        var rebased = ExchangeRatesBundle(base: currency, actualDate: self.date)
        rebased.setRate(1 / directRate, forCurrency: self.baseCurrency)
        rates.forEach { (code, rate) in
            let convertedRate = rate / directRate
            rebased.setRate(convertedRate, forCurrency: code)
        }
        
        return rebased
    }
    
    /**
     * Check if the two exchange rates bundles are different in their contents.
     *
     * In order for any two bundles to be "equal", they must have the same base currency
     *  as well as the list of rated currencies.
     *
     * - Returns `false` if the two exchange rates bundles are equal *in terms of contents*,
     *  otherwise `true`
     */
    static func diff(lhs: ExchangeRatesBundle, rhs: ExchangeRatesBundle) -> Bool {
        guard lhs.baseCurrency == rhs.baseCurrency else {
            return true
        }
        
        guard lhs.rates.keys == rhs.rates.keys else {
            return true
        }
        
        return false
    }
}
