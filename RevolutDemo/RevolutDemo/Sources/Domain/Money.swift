import Foundation

///Money representation.
struct Money {
    var amount: Double
    let currency: CurrencyInfo
}
