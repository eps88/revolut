import Foundation

/// Currency description.
struct CurrencyInfo {
    
    let code: String
    
    let name: String
    
    let symbol: String
}
