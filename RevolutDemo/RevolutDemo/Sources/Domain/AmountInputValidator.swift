import Foundation

/**
 * An implementation of `InputValidator` to specifically validate amount input from a user.
 */
final class AmountInputValidator: InputValidator {
    
    static let defaultDelimeterChar: Character = "."
    static let defaultMaxFractionalLength: UInt8 = 2
    
    private(set) var maxFractionalDigits: Int
    private(set) var amountLimitation: Double?
    private(set) var maxIntegerDigits: Int?
    
    /**
     * Initializes the validator with additional amount and precision restrictions.
     *
     * - Parameters:
     *   - amountLimit: Maximum value to be allowed by the validator. When provided, must be
     *      positive, the default value is `nil`.
     *   - fracMaxLength: Number of digits allowed to follow the decimal separator.
     *      By default two digits are allowed for the fractional part.
     */
    init?(with amountLimit: Int? = nil, fracMaxLength: UInt8 = defaultMaxFractionalLength) {
        self.maxFractionalDigits = Int(fracMaxLength)
        
        if let limit = amountLimit {
            guard limit > 0 else {
                return nil
            }
            
            self.amountLimitation = Double(limit)
            
            let printedLimit = String(limit)
            self.maxIntegerDigits = printedLimit.count
        }
    }
    
    func isValidInput(_ inputString: String) -> Bool {
        let amountInput = normalizedAmountString(from: inputString)
        
        guard !hasInvalidSymbols(amountInput) else {
            return false
        }
        
        if let startingChar = amountInput.first {
            if startingChar == AmountInputValidator.defaultDelimeterChar {
                return false
            }
        }
        
        guard leadingZeroIsFollowedByDelimeter(amountInput) else {
            return false
        }
        
        guard meetsValueLimitation(amountInput) else {
            return false
        }
        
        if let delimeterIndex = amountInput.firstIndex(of: AmountInputValidator.defaultDelimeterChar) {
            if let intMaxLength = maxIntegerDigits {
                guard amountInput.distance(from: amountInput.startIndex, to: delimeterIndex) <= intMaxLength else {
                    return false
                }
            }
            
            guard amountInput.distance(from: delimeterIndex, to: amountInput.endIndex) <= maxFractionalDigits + 1 else {
                return false
            }
            
            guard let lastDelimeterIndex = amountInput.lastIndex(of: AmountInputValidator.defaultDelimeterChar),
                lastDelimeterIndex == delimeterIndex else {
                return false
            }
        } else {
            if let intMaxLength = maxIntegerDigits {
                return amountInput.count <= intMaxLength
            }
        }
        
        return true
    }
    
    private func hasInvalidSymbols(_ amountString: String) -> Bool {
        var allowedSymbols = CharacterSet.decimalDigits
        allowedSymbols.insert(charactersIn: "\(AmountInputValidator.defaultDelimeterChar)")
        
        return amountString.rangeOfCharacter(from: allowedSymbols.inverted) != nil
    }
    
    private func leadingZeroIsFollowedByDelimeter(_ amountString: String) -> Bool {
        if amountString.count > 1 {
            let firstChar = amountString[amountString.startIndex]
            if firstChar == "0" {
                let secondIndex = amountString.index(amountString.startIndex, offsetBy: 1)
                let secondChar = amountString[secondIndex]
                
                return secondChar == AmountInputValidator.defaultDelimeterChar
            }
        }
        return true
    }
    
    private func meetsValueLimitation(_ amountString: String) -> Bool {
        guard !amountString.isEmpty else {
            return true
        }
        
        guard let amountValue = Double(amountString) else {
            return false
        }
        
        if let limit = amountLimitation {
            guard amountValue <= limit else {
                return false
            }
        }
        
        return true
    }
    
    private func normalizedAmountString(from inputString: String) -> String {
        let validSeparator = AmountInputValidator.defaultDelimeterChar == "." ? "." : ","
        let invalidSeparator = AmountInputValidator.defaultDelimeterChar == "." ? "," : "."
        
        let normalizedInput = inputString.replacingOccurrences(of: invalidSeparator, with: validSeparator)
        
        return normalizedInput
    }
    
}
