import Foundation

final class CommonMoneyFormatter: MoneyFormatter {
    private let defaultAmountStringFormat = "%.2f"
    
    func amountString(from amount: Double) -> String {
        let amountAsString = String(format: defaultAmountStringFormat, amount)
        return amountAsString
    }
    
    func moneyString(from money: Money) -> String {
        return amountString(from: money.amount)
    }
    
}
