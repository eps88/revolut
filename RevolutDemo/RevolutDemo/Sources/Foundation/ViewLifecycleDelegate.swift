import Foundation

/**
 * An interface for a delegate interested in view lifecycle events.
 */
protocol ViewLifecycleDelegate: class {
    
    func viewDidLoad()
    
    func viewWillAppear(_ animated: Bool)
    
    func viewWillDisappear(_ animated: Bool)

}

extension ViewLifecycleDelegate {
    
    func viewDidLoad() {}
    
    func viewWillAppear(_ animated: Bool) {}
    
    func viewWillDisappear(_ animated: Bool) {}
    
}
