import Foundation

final class TrackingService: TrackingContract {
    private let TAG = String(describing: TrackingService.self)
    
    private static let defaultUpdateInterval = 1.0
    private static let defaultTimingAccuracy = 125
    
    private var state: TrackingState = .suspended
    var isRunning: Bool {
        return state == .resumed
    }
    
    ///Repeating timer dispatch source.
    private var timer: DispatchSourceTimer
    
    ///A dedicated serial dispatch queue to handle timer events.
    fileprivate let timingQueue = DispatchQueue(
        label: "com.revolut.demo.tracking",
        qos: .utility
    )
    
    ///The attached service to be tracked.
    var linkedService: ExchangeRatesServiceApi?
    
    /**
     * Initialize tracking of the linked service with the certain time interval.
     *
     * - Parameters:
     *   - interval: The repeating interval for the timer, in seconds
     */
    init(repeating interval: TimeInterval) {
        logD(TAG, message: "init", logLevel: .verbose)
        
        self.timer = DispatchSource.makeTimerSource(queue: timingQueue)
        timer.schedule(deadline: .now(), repeating: interval, leeway: .milliseconds(TrackingService.defaultTimingAccuracy))
        timer.setEventHandler(handler: { [unowned self] in
            self.onTimer()
        })
    }
    
    deinit {
        logD(TAG, message: "destroy", logLevel: .verbose)
        
        timer.setEventHandler(handler: nil)
        timer.cancel()
    }
    
    func start() {
        guard !isRunning else {
            return
        }
        
        logD(TAG, message: "start ▶️", logLevel: .verbose)
        timingQueue.async {
            self.state = .resumed
            self.timer.resume()
        }
    }
    
    func stop() {
        guard isRunning else {
            return
        }
        
        logD(TAG, message: "stop ⏹", logLevel: .verbose)
        timingQueue.async {
            self.state = .suspended
            self.timer.suspend()
        }
    }
    
    private func onTimer() {
        logD(TAG, message: "onTimer 🔁, on thread:  \(Thread.current)", logLevel: .verbose)
        
        linkedService?.loadLatestRates(with: "EUR")
    }
    
}
