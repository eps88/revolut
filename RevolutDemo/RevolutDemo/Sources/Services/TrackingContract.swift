import Foundation

enum ExchangeRatesTrackingEvent: String {
    case ratesUpdated = "com.revolut.demo.conv.ratesUpdated"
}

enum TrackingState {
    case suspended
    case resumed
}

protocol TrackingContract: class {
    
    var isRunning: Bool { get }
    
    func start()
    
    func stop()
}
