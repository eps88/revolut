import Foundation

class ExchangeRatesWebService: ExchangeRatesServiceApi {
    private let TAG = String(describing: ExchangeRatesWebService.self)
    
    weak var listener: ExchangeRatesFetchingListener?
    
    private let baseUrl: String
    
    private lazy var session: URLSession = {
        let configuration = URLSessionConfiguration.ephemeral
        configuration.waitsForConnectivity = true
        configuration.timeoutIntervalForRequest = 0.5
        configuration.timeoutIntervalForResource = 0.5
        return URLSession(configuration: configuration, delegate: nil, delegateQueue: nil)
    }()
    
    //MARK: - Lifecycle
    
    init(with url: String) {
        logD(TAG, message: "init", logLevel: .verbose)
        self.baseUrl = url
    }
    
    deinit {
        logD(TAG, message: "destroy", logLevel: .verbose)
    }

    //MARK: - Service API
    
    func loadLatestRates(with base: String) {
        logD(TAG, message: "Fetching latest rates ...", logLevel: .verbose)
        
        let ratesUrlString = baseUrl + RevolutApi.live + "?base=\(base)"
        guard let ratesUrl = URL(string: ratesUrlString) else {
            logD(TAG, message: "Failed: bad url!")
            return
        }
        
        let task = session.dataTask(with: ratesUrl) { [weak self] (data, response, error) in
            self?.handleResponse(data: data, response: response, error: error)
        }
        task.resume()
    }
    
    //MARK: - Internals
    
    private func handleResponse(data: Data?, response: URLResponse?, error: Error?) {
        if let error = error {
            logD(TAG, message: "Failed loading rates: \(error.localizedDescription)", logLevel: .warning)
            listener?.onFailure(error: error)
            return
        }
        
        guard let httpResponse = response as? HTTPURLResponse else {
            listener?.onFailure(error: nil)
            return
        }
        
        guard (200...299).contains(httpResponse.statusCode) else {
            logD(TAG, message: "Failed loading rates: \(httpResponse.statusCode)", logLevel: .warning)
            listener?.onFailure(error: nil)
            return
        }
        
        if let jsonData = data,
            let json = try? JSONSerialization.jsonObject(with: jsonData, options: []),
            let jsonAsDictionary = json as? [String: Any],
            let latestRates = LiveRatesResult(json: jsonAsDictionary) {
            logD(self.TAG, message: "Response: \(latestRates)", logLevel: .verbose)
            
            listener?.onSuccess(result: latestRates)
        } else {
            logD(self.TAG, message: "Failed reading response data!", logLevel: .warning)
            listener?.onFailure(error: nil)
        }
    }
    
}
