import Foundation

/**
 * Defines the resources (services) available on the API.
 */
struct RevolutApi {
    static let baseUrl = "https://revolut.duckdns.org"
    
    static let live = "/latest"
}
