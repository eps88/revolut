import Foundation

protocol ExchangeRatesServiceApi: class {
    
    /**
     * Get the most recent exchange rates given the base currency.
     *
     * - Parameter base: Currency code (as per ISO 4217) for the currency
     *      to which all exchange rates are relative.
     */
    func loadLatestRates(with base: String)
    
}

protocol ExchangeRatesFetchingListener: class {
    
    func onSuccess(result: LiveRatesResult)
    
    func onFailure(error: Error?)
    
}
