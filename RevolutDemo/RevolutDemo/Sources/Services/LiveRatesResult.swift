import Foundation

/**
 * A struct to represent the result of requesting latest exchange rates.
 */
struct LiveRatesResult {
    static let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd"
        return formatter
    }()
    
    let latest: ExchangeRatesBundle
}

/**
 * The expected JSON-keys for a live-rates response.
 */
enum LiveRatesResponseKey: String {
    case base = "base"
    case date = "date"
    case rates = "rates"
}

extension LiveRatesResult {
    
    init?(json: [String: Any]) {
        
        guard let baseCurrency = json[LiveRatesResponseKey.base.rawValue] as? String else {
            return nil
        }
        
        guard let timeValue = json[LiveRatesResponseKey.date.rawValue] as? String,
            let time = LiveRatesResult.dateFormatter.date(from: timeValue) else {
                return nil
        }
        
        guard let rates = json[LiveRatesResponseKey.rates.rawValue] as? [String: Any] else {
            return nil
        }
        
        var bundle = ExchangeRatesBundle(base: baseCurrency, actualDate: time)
        
        for (k, v) in rates {
            guard let rateValue = v as? Double else {
                continue
            }
            bundle.setRate(rateValue, forCurrency: k)
        }
        
        self.latest = bundle
    }
    
}
