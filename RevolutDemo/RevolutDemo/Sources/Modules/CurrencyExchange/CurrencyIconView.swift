import UIKit

@IBDesignable
final class CurrencyIconView: UIView {
    private let TAG = String(describing: CurrencyIconView.self)
    
    static let defaultEdgingColour = UIColor.darkGray
    static let highlightedEdgingColour = UIColor(red: 70.0 / 255.0, green: 185.0 / 255.0, blue: 120.0 / 255.0, alpha: 1.0)

    @IBOutlet var iconContentView: UIView!
    @IBOutlet weak var symLabel: UILabel!
    
    var edgingColour: UIColor = CurrencyIconView.defaultEdgingColour {
        didSet {
            redraw(within: self.bounds)
        }
    }
    
    private var drawingLayer: CAShapeLayer!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        let nibName = String(describing: CurrencyIconView.self)
        Bundle.main.loadNibNamed(nibName, owner: self, options: nil)
        
        self.addSubview(iconContentView)
        
        iconContentView.translatesAutoresizingMaskIntoConstraints = false
        iconContentView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        iconContentView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        iconContentView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        iconContentView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
    }
    
    override func draw(_ rect: CGRect) {
        redraw(within: rect)
    }
    
    func setSymbol(_ symbol: String) {
        symLabel.text = symbol
    }
    
    private func redraw(within rect: CGRect) {
        logD(TAG, message: "redraw", logLevel: .verbose)
        let lineWidth = CGFloat(0.5)
        
        let vWidth = rect.width
        let vHeight = rect.height
        
        let edgingCenter = CGPoint(x: vWidth / 2, y: vHeight / 2)
        let edgingRadius = (vWidth - lineWidth) / 2
        let edgingPath = UIBezierPath(arcCenter: edgingCenter,
                                      radius: edgingRadius,
                                      startAngle: CGFloat(0),
                                      endAngle:CGFloat(Double.pi * 2),
                                      clockwise: true)
        
        drawingLayer?.removeFromSuperlayer()
        drawingLayer = CAShapeLayer()
        drawingLayer.path = edgingPath.cgPath
        drawingLayer.bounds = edgingPath.cgPath.boundingBox
        drawingLayer.position = edgingCenter
        drawingLayer.strokeColor = edgingColour.cgColor
        drawingLayer.fillColor = UIColor.clear.cgColor
        drawingLayer.lineWidth = lineWidth
        
        self.layer.addSublayer(drawingLayer)
    }
}
