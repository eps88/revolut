import UIKit

protocol CurrencyConversionTableDelegate: class {
    
    func didChangeInput(at index: Int, to newValue: String)
    
    func didSelectItem(at index: Int)
}

final class CurrencyExchangeTableSource: NSObject {
    private let TAG = String(describing: CurrencyExchangeTableSource.self)
    
    private let cellNibName = "CurrencyValueCell"
    private let cellIdentifier = "rateCell"
    private let cellHeight = 100
    
    private let iconNameSuffix = "_icon"
    
    weak private(set) var tableView: UITableView!
    weak var delegate: CurrencyConversionTableDelegate?
    
    private var inputValidator: InputValidator?
    
    private var items: [CurrencyValueItem] = []
    
    init(forTable tableView: UITableView, inputValidator: InputValidator? = nil) {
        super.init()
        
        self.tableView = tableView
        self.inputValidator = inputValidator
        
        tableView.register(UINib(nibName: cellNibName, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        tableView.estimatedRowHeight = CGFloat(cellHeight)
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func update(_ items: [CurrencyValueItem], reset: Bool) {
        let shouldReload = reset || (self.items.count != items.count)
        self.items = items
        
        if shouldReload {
            tableView.reloadData()
        } else {
            //Do not interfere with possibly heavy scrolling
            let shouldSkipUpdate = tableView.isDecelerating
            logD(TAG, message: "Will skip updating due to intense scrolling: \(shouldSkipUpdate)", logLevel: .verbose)
            if shouldSkipUpdate { return }
            
            if let visiblePaths = tableView.indexPathsForVisibleRows {
                //Avoid reloading the "master" row to keep its first-responder status, while active
                let slaveVisiblePaths = visiblePaths.filter { $0.row != 0 }
                //Go for reloading cells, as in-place updates are not reliable due to possible scrolling
                tableView.reloadRows(at: slaveVisiblePaths, with: .none)
            }
        }
    }
    
    func rearrangeToTop(from index: Int) {
        guard items.startIndex ..< items.endIndex ~= index  else {
            return
        }
        
        guard index > 0 else {
            triggerEditing()
            return
        }
        
        let topmostIndexPath = IndexPath(row: 0, section: 0)
        let selectionIndexPath = IndexPath(row: index, section: 0)
        
        let topItem = items.remove(at: selectionIndexPath.row)
        items.insert(topItem, at: topmostIndexPath.row)
        
        if let visiblePaths = tableView.indexPathsForVisibleRows {
            //Gracefully move the selected item only if it makes sense
            if visiblePaths.contains(topmostIndexPath) {
                //Hook up the first responder status and update highlighting right away
                let nextTopmostItem = tableView.cellForRow(at: selectionIndexPath) as? CurrencyValueCell
                nextTopmostItem?.setBadgeTintColour(CurrencyIconView.highlightedEdgingColour)
                nextTopmostItem?.setEditable(true)
                //Resign the base status
                let formerTopmostItem = tableView.cellForRow(at: topmostIndexPath) as? CurrencyValueCell
                formerTopmostItem?.setBadgeTintColour(CurrencyIconView.defaultEdgingColour)
                formerTopmostItem?.setEditable(false)
                
                self.tableView.performBatchUpdates({
                    self.tableView.moveRow(at: selectionIndexPath, to: topmostIndexPath)
                }) { _ in
                    //Adjust scroll position if not fully visible
                    let topmostItemRect = self.tableView.rectForRow(at: topmostIndexPath)
                    let topmostFullyVisible = self.tableView.bounds.contains(topmostItemRect)
                    if !topmostFullyVisible {
                        UIView.animate(withDuration: 0.25, animations: {
                            self.tableView.scrollToRow(at: topmostIndexPath, at: .top, animated: false)
                        })
                    }
                }
            } else {
                //Animate manually unless a completion-aware version of `scrollToRow` is provided
                let forceScrollingDuration = 0.25
                UIView.animate(withDuration: forceScrollingDuration, animations: {
                    self.tableView.scrollToRow(at: topmostIndexPath, at: .top, animated: false)
                }) { _ in
                    if let visiblePaths = self.tableView.indexPathsForVisibleRows {
                        //Assure the displayed content is up-to-date
                        self.tableView.reloadRows(at: visiblePaths, with: .none)
                    }
                    self.triggerEditing()
                }
            }
        }
    }
    
    private func triggerEditing() {
        let topmostIndexPath = IndexPath(row: 0, section: 0)
        let topmostItem = self.tableView.cellForRow(at: topmostIndexPath) as? CurrencyValueCell
        topmostItem?.setEditable(true)
    }
    
}

extension CurrencyExchangeTableSource: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(cellHeight)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.didSelectItem(at: indexPath.row)
    }
    
}

extension CurrencyExchangeTableSource: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? CurrencyValueCell else {
            return UITableViewCell()
        }
        
        cell.selectionStyle = .none
        
        let item = items[indexPath.row]
        
        cell.setBadgeTintColour(indexPath.row == 0 ?
            CurrencyIconView.highlightedEdgingColour:
            CurrencyIconView.defaultEdgingColour)
        cell.setCurrencySymbol(item.currency.symbol)
        cell.setCurrencyCode(item.currency.code)
        cell.setCurrencyName(item.currency.name)
        cell.setCurrencyValue(item.value)
        cell.setEditable(indexPath.row == 0)
        cell.editingTag = indexPath.row
        cell.editingDelegate = self
        
        return cell
    }
    
}

extension CurrencyExchangeTableSource: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let currentText = textField.text, let replacementRange = Range(range, in: currentText) else {
            return false
        }
        
        var validationResult = true
        let inputCandidate = currentText.replacingCharacters(in: replacementRange, with: string)
        if let validator = inputValidator {
            validationResult = validator.isValidInput(inputCandidate)
        }
        
        if validationResult {
            let editingTag = textField.tag
            delegate?.didChangeInput(at: editingTag, to: inputCandidate)
        }
        
        return validationResult
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
}
