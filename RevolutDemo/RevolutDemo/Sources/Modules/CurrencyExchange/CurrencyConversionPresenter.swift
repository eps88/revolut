import Foundation

final class CurrencyConversionPresenter: CurrencyExchangePresenter {
    private let TAG = String(describing: CurrencyConversionPresenter.self)
    
    private static let defaultRatesUnavailableText = "Something went wrong. Please try again later."
    private static let defaultBaseAmount = 100.0
    
    //MARK: - Dependencies
    
    private weak var view: CurrencyExchangeView!
    
    private var amountFormatter: AmountFormatter
    
    private var trackingService: TrackingContract
    
    //MARK: - Presentation Data
    
    private var baseRates: ExchangeRatesBundle?
    private var conversionRates: ExchangeRatesBundle?
    
    private var baseAmount: Double
    private var recentAmountInput: String {
        didSet {
            if let newBaseAmount = Double(recentAmountInput) {
                baseAmount = newBaseAmount
                logD(TAG, message: "Amount value did change to: \(newBaseAmount)", logLevel: .verbose)
            }
        }
    }
    
    private var displayItems: [CurrencyValueItem] = []
    
    //MARK: - Initialization
    
    init(view: CurrencyExchangeView,
         amountFormatter: AmountFormatter,
         trackingService: TrackingContract) {
        
        self.view = view
        self.amountFormatter = amountFormatter
        self.trackingService = trackingService
        
        self.baseAmount = CurrencyConversionPresenter.defaultBaseAmount
        self.recentAmountInput = amountFormatter.amountString(from: baseAmount)
    }
    
    //MARK: - Internals
    
    private func doTrackExchangeRates() {
        view.showEmpty()
        
        trackingService.start()
    }
    
    private func doStopTrackingExchangeRates() {
        trackingService.stop()
    }
    
    private func didUpdateCurrencyExchangeRates(_ rates: ExchangeRatesBundle, shouldReset: Bool) {
        guard rates.count > 0 else {
            return
        }
        
        self.baseRates = rates
        
        if let baseCurrency = conversionRates?.baseCurrency {
            self.conversionRates = rates.rebased(to: baseCurrency)
        } else {
            self.conversionRates = rates
        }
        
        if (shouldReset) {
            rebuildConversionList()
        } else {
            updateConversionList()
        }
        
        self.view.updateConversionValues(with: self.displayItems, animated: shouldReset)
    }
    
    private func didFailUpdatingCurrencyExchangeRates() {
        //TODO: show an error
    }
    
    private func updateConversionList() {
        guard let actualRates = conversionRates else {
            return
        }
        
        for (index, item) in displayItems.enumerated() {
            let itemCode = item.currency.code
            if itemCode == actualRates.baseCurrency {
                displayItems[index].value = recentAmountInput
            } else if let itemRate = actualRates.getRate(forCurrency: itemCode) {
                let itemValue = baseAmount * itemRate
                let itemValueString = amountFormatter.amountString(from: itemValue)
                displayItems[index].value = itemValueString
            } else {
                continue
            }
        }
    }
    
    private func rebuildConversionList() {
        displayItems.removeAll()
        
        guard let baseCurrency = conversionRates?.baseCurrency,
            let baseCurrencyInfo = CurrencyCatalogue.currencyByCode(baseCurrency),
            let availableCurrencies = conversionRates?.rates else {
                return
        }
        
        let topmostItem = CurrencyValueItem(currency: baseCurrencyInfo, value: recentAmountInput)
        displayItems.append(topmostItem)
        
        for (code, rate) in availableCurrencies {
            let currencyValue = baseAmount * rate
            let currencyValueString = amountFormatter.amountString(from: currencyValue)
            if let currencyInfo = CurrencyCatalogue.currencyByCode(code) {
                let item = CurrencyValueItem(currency: currencyInfo, value: currencyValueString)
                displayItems.append(item)
            }
        }
    }
    
}

extension CurrencyConversionPresenter: CurrencyExchangeViewDelegate {
    
    func viewDidLoad() { }
    
    func viewWillAppear(_ animated: Bool) {
        doTrackExchangeRates()
    }
    
    func viewWillDisappear(_ animated: Bool) {
        doStopTrackingExchangeRates()
    }
    
    func doRetryLoadingExchangeRates() {
        doTrackExchangeRates()
    }
    
    func amountInputDidChange(_ value: String) {
        logD(TAG, message: "Amount input did change to: \(value)", logLevel: .verbose)
        let newValue = !value.isEmpty ? value : "0"
        guard let _ = Double(newValue) else {
            return
        }
        
        self.recentAmountInput = newValue
        updateConversionList()
        view.updateConversionValues(with: displayItems, animated: false)
    }
    
    func didSelectBaseCurrency(at index: Int) {
        guard displayItems.startIndex ..< displayItems.endIndex ~= index  else {
            return
        }
        
        guard index > 0 else {
            view.handleShouldBeginEditing()
            return
        }
        
        let newBase = displayItems[index]
        if let _ = Double(newBase.value),
            let actualRates = baseRates,
            let rebasedRates = actualRates.rebased(to: newBase.currency.code) {
            logD(TAG, message: "Will re-base to: \"\(newBase.currency.code)\", at index: \(index)", logLevel: .verbose)
            
            self.recentAmountInput = newBase.value
            self.conversionRates = rebasedRates
            
            let newBaseItem = displayItems.remove(at: index)
            displayItems.insert(newBaseItem, at: 0)
            
            view.handleDidChangeBaseCurrency(to: newBase.currency.code, at: index)
        }
    }
    
}

extension CurrencyConversionPresenter: ExchangeRatesFetchingListener {
    
    func onSuccess(result: LiveRatesResult) {
        logD(TAG, message: "onSuccess")
        
        guard trackingService.isRunning else {
            logD(TAG, message: "Ignored, as tracking is not active")
            return
        }
        
        var hasChanged = true
        if let existingRates = baseRates {
            hasChanged = ExchangeRatesBundle.diff(lhs: existingRates, rhs: result.latest)
        }
        
        logD(TAG, message: "Needs hard reset: \(hasChanged)", logLevel: .verbose)
        DispatchQueue.main.async {
            self.didUpdateCurrencyExchangeRates(result.latest, shouldReset: hasChanged)
        }
    }
    
    func onFailure(error: Error?) {
        logD(TAG, message: "onFailure")
        
        if let _ = baseRates {
            return //For simplicity, ignore and let user interact with what she has
        }
        
        doStopTrackingExchangeRates()
        DispatchQueue.main.async {
            self.view.showError(text: CurrencyConversionPresenter.defaultRatesUnavailableText)
        }
    }
    
}
