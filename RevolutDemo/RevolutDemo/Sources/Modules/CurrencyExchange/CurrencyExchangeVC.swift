import UIKit

final class CurrencyExchangeVC: UIViewController, CurrencyExchangeView {
    private let TAG = String(describing: CurrencyExchangeVC.self)
    
    private let defaultFadeDuration = 0.125
    
    var actionHandler: CurrencyExchangeViewDelegate?
    
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var errorNote: UILabel!
    @IBOutlet weak var retryButton: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    private var tableViewDataSource: CurrencyExchangeTableSource!
    private var activityIndicator: UIActivityIndicatorView!
    
    //MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUi()
        
        actionHandler?.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        actionHandler?.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        actionHandler?.viewWillDisappear(animated)
    }
    
    //MARK: - View contract
    
    func setTitle(text: String?) {
        self.navigationItem.title = text
    }
    
    func switchActivity(on: Bool) {
        if on {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
    }
    
    func showEmpty() {
        switchActivity(on: true)
        
        tableView.isHidden = true
        errorView.isHidden = true
    }
    
    func showError(text: String) {
        switchActivity(on: false)
        
        tableView.isHidden = true
        
        errorView.isHidden = false
        errorNote.text = text
        retryButton.isEnabled = true
    }
    
    func updateConversionValues(with items: [CurrencyValueItem], animated: Bool) {
        logD(TAG, message: "Updating values, reset: \(animated)", logLevel: .verbose)
        switchActivity(on: false)
        
        errorView.isHidden = true
        tableView.isHidden = false
        
        if animated {
            contentFadeOut {
                self.tableViewDataSource.update(items, reset: animated)
                self.contentFadeIn()
            }
        } else {
            tableViewDataSource.update(items, reset: animated)
        }
    }
    
    func handleShouldBeginEditing() {
        tableViewDataSource.rearrangeToTop(from: 0)
    }
    
    func handleDidChangeBaseCurrency(to currency: String, at index: Int) {
        tableViewDataSource.rearrangeToTop(from: index)
    }
    
    //MARK: - User actions

    @IBAction func onRetryButtonClick(_ sender: Any) {
        actionHandler?.doRetryLoadingExchangeRates()
    }
    
    //MARK: - Internals
    
    private func configureUi() {
        configureTableView()
        configureActivityIndication()
        configureErrorView()
    }
    
    private func configureTableView() {
        let inputValidator = AmountInputValidator(with: nil, fracMaxLength: 4)
        let tableSource = CurrencyExchangeTableSource(forTable: tableView, inputValidator: inputValidator)
        tableSource.delegate = self
        self.tableViewDataSource = tableSource
        
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.keyboardDismissMode = .onDrag
    }
    
    private func configureActivityIndication() {
        self.activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        self.activityIndicator.color = UIColor.darkGray
        let barButton = UIBarButtonItem(customView: activityIndicator)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    private func configureErrorView() { }

    private func contentFadeIn() {
        UIView.animate(withDuration: defaultFadeDuration) {
            self.tableView.alpha = 1.0
        }
    }
    
    private func contentFadeOut(_ completion: @escaping () -> Void) {
        UIView.animate(withDuration: defaultFadeDuration, animations: {
            self.tableView.alpha = 0.0
        }, completion: { _ in
            completion()
        })
    }
    
}

extension CurrencyExchangeVC: CurrencyConversionTableDelegate {
    
    func didSelectItem(at index: Int) {
        actionHandler?.didSelectBaseCurrency(at: index)
    }
    
    func didChangeInput(at index: Int, to newValue: String) {
        actionHandler?.amountInputDidChange(newValue)
    }
}
