import Foundation

protocol CurrencyExchangeView: class {
    
    var actionHandler: CurrencyExchangeViewDelegate? { get set }
    
    func setTitle(text: String?)
    
    func switchActivity(on: Bool)
    
    func showEmpty()
    
    func showError(text: String)
    
    func updateConversionValues(with items: [CurrencyValueItem], animated: Bool)
    
    func handleShouldBeginEditing()
    
    func handleDidChangeBaseCurrency(to currency: String, at index: Int)
    
}
