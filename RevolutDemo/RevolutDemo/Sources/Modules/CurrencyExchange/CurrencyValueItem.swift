import Foundation

/// View model of currency value within a list.
struct CurrencyValueItem {
    let currency: CurrencyInfo
    var value: String
}
