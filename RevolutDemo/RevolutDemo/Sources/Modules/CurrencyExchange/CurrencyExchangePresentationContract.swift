import Foundation

protocol CurrencyExchangeViewDelegate: ViewLifecycleDelegate {
    
    func doRetryLoadingExchangeRates()
    
    func amountInputDidChange(_ value: String)
    
    func didSelectBaseCurrency(at index: Int)
}

protocol CurrencyExchangePresenter: class {
    
}


