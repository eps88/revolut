import Foundation

/**
 * Convenient awake initializer of the module.
 *
 * The whole module hierarchy is assembled for the instantiated view controller.
 */
final class CurrencyExchangeModuleAwakeInitializer: NSObject {
    private let TAG = String(describing: CurrencyExchangeModuleAwakeInitializer.self)
    
    @IBOutlet weak var viewController: CurrencyExchangeVC!
    
    override func awakeFromNib() {
        logD(TAG, message: "Awake from nib ...", logLevel: .verbose)
        
        let assembly = CurrencyExchangeModuleAssembly()
        assembly.buildModule(around: viewController)
    }
}
