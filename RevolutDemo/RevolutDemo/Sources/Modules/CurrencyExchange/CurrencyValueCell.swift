import UIKit

final class CurrencyValueCell: UITableViewCell {

    @IBOutlet weak var currencyContainer: UIStackView!
    @IBOutlet weak var iconView: CurrencyIconView!
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var valueInput: UITextField!
    
    var editingDelegate: UITextFieldDelegate? {
        didSet {
            valueInput.delegate = editingDelegate
        }
    }
    
    var editingTag: Int = 0 {
        didSet {
            valueInput.tag = tag
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        configureCell()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setBadgeTintColour(_ colour: UIColor) {
        iconView.edgingColour = colour
    }
    
    func setCurrencySymbol(_ symbol: String) {
        iconView.setSymbol(symbol)
    }
    
    func setCurrencyCode(_ code: String) {
        codeLabel.text = code
    }
    
    func setCurrencyName(_ name: String) {
        nameLabel.text = name
    }
    
    func setCurrencyValue(_ value: String) {
        valueInput.text = value
    }
    
    func setEditable(_ isEditable: Bool) {
        valueInput.isEnabled = isEditable
        if (isEditable) {
            valueInput.becomeFirstResponder()
        }
    }
    
    private func configureCell() {
        self.currencyContainer.layoutMargins = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
        self.currencyContainer.isLayoutMarginsRelativeArrangement = true
        
        self.valueInput.keyboardType = .decimalPad
    }
    
}
