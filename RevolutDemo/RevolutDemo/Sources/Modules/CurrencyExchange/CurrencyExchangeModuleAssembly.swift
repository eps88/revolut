import Foundation

/**
 * Converter's module assembly implementation.
 *
 * To keep it simple, skip any complex dependency management for now. Hence assume that an assembly
 *  is allowed to directly instantiate any required services, instead of obtaining them via
 *  a provided resolver interface.
 */
final class CurrencyExchangeModuleAssembly: CurrencyExchangeModuleAssembler {
    
    func buildModule(around view: CurrencyExchangeView) {
        
        let webService = ExchangeRatesWebService(with: RevolutApi.baseUrl)
        let trackingService = TrackingService(repeating: 1.0)
        trackingService.linkedService = webService
        
        let amountFormatter = CommonMoneyFormatter()
        
        let presenter = CurrencyConversionPresenter(view: view,
                                                    amountFormatter: amountFormatter,
                                                    trackingService: trackingService)
        view.actionHandler = presenter
        webService.listener = presenter
    }
}
