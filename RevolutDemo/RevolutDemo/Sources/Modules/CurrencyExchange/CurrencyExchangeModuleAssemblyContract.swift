import Foundation

/**
 * The converter's module assembly contract.
 */
protocol CurrencyExchangeModuleAssembler: class {
    
    func buildModule(around view: CurrencyExchangeView)
}
